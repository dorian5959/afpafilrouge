<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css du site.css">
		<link rel="stylesheet" type="text/css" href="css/w3-theme-blue-grey.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<title>Forgotten Password</title>
	</head>


<body class="w3-theme-d1">
	<div>
		<header>
			<div id="titre_principale">
				<h1>Forgotten Password</h1>
	</div>
			
<form action="forgot.do" method="post">
	<div id="formulaire">
	<p>
	<table>
		<label for="email"> Email adress :</label>
		<input type="text" name="email" id="email" placeholder="Ex : jojolartichaud@gmail.com" size="30" maxlength="50">

		<br />
		<label for="login"> Login :</label>
		<input type="text" name="login" id="login" placeholder="Ex : Login" size="30" maxlength="50">
		<br />
		<br />
		<input class="refresh" type="submit" name="Envoyer" />
	</table>
	</p>
</form>
	<a href="login.do"> Return home</a>
	</div>
</body>
</html>