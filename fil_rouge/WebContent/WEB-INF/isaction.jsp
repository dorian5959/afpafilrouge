<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@taglib  uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css du site.css">
		<link rel="stylesheet" type="text/css" href="css/w3-theme-blue-grey.css">
		<title>Reset</title>
	</head>


<body class="w3-theme-d1">
						<%@ page import="java.util.*"
						import="com.afpa.idp.dao.*"
						import="com.afpa.idp.classes.*"
						import= "java.sql.*"
						import="org.json.JSONException"%>
						
	<div>

		<h1>
		
		<c:if test="${not empty request}">
			<c:choose>
				<c:when test="${request=='forgottentrue'}">
			    	<c:out value="Password reset, an email has been sent !"/>
				</c:when>
				<c:when test="${request=='forgottenfalse'}">
			    	<c:out value="Wrong email or login !"/>
				</c:when>
				
				<c:when test="${request=='decotrue'}">
					<c:out value="Log out successful"/>
				</c:when>
				<c:when test="${request=='inscritrue'}">
					<c:out value="Sign up successful !"/>
				</c:when>
				<c:when test="${request=='inscrifalse'}">
					<c:out value="User already exists, please use another User Name"/>
				</c:when>
				<c:when test="${request=='connectedfalse'}">
					<c:out value="You  re not logged in"/>
				</c:when>
			</c:choose>
		</c:if>
		
		
		</h1><br>
	</div>
	
	<a href="login.do"> Return home</a>
</body>
</html>