<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" type="text/css" href="css/css du site.css">
		<link rel="stylesheet" type="text/css" href="css/w3-theme-blue-grey.css">
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<title>Sign In</title>
	</head>


<body class="w3-theme-d1">
	<div>
		<header>
			<div id="titre_principale">
			<img class="logo" src="https://cdn.discordapp.com/attachments/572759656053866527/574984105788440577/logo.png" alt="Logo" />
				<h1>Sign In</h1>
	</div>
			
<form action="connected.do" method="post">
	<div id="formulaire">
	<p>
	<table>
		<label for="login"> Login :</label>
		<input type="text" name="username" id="username" placeholder="Ex : Login" size="30" maxlength="50">

		<br />
		<label for="pass"> Password :</label>
		<input type="Password" name="pass" id="pass"
		placeholder="Ex : Password" size="30" maxlength="50" />
		<br />
		<br />
		<input class="refresh" type="submit" name="Envoyer" />
	</table>
	</p>
</form>

<a href="inscription.do"> Sign Up </a>
<br />
<a href="forgot.do"> Password forgotten ? </a>
	</div>
</body>
</html>