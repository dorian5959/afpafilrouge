package com.afpa.idp.classes;

import java.util.Date;

public class Client {

		private int id;
		private String name;
		private String firstname;
		private String mail;
		private String phone;
		private Date birthdate;
		private String address;
		private int zipcode;
		private String ville;
		private String pays;
		private User user;
		
		public Client(int id, String n, String f, String m, String p, Date d, String a, int z, String v, String pa, User user) {
			
			this.id=id;
			this.name = n;
			this.firstname = f;
			this.mail = m;
			this.phone = p;
			this.birthdate = d;
			this.address = a;
			this.zipcode = z;
			this.ville = v;
			this.pays = pa;
			this.user=user;
		}

		public Client () {
			
		}
		
		public Client(String n, String f, String m, String p, Date d, String a, int z, String v, String pa, User user) {
			
			this.name = n;
			this.firstname = f;
			this.mail = m;
			this.phone = p;
			this.birthdate = d;
			this.address = a;
			this.zipcode = z;
			this.ville = v;
			this.pays = pa;
			this.user=user;
		}

		public  int getId() {
			return id;
		}

		public  void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getFirstname() {
			return firstname;
		}

		public void setFirstname(String firstname) {
			this.firstname = firstname;
		}

		public String getMail() {
			return mail;
		}

		public void setMail(String mail) {
			this.mail = mail;
		}

		public String getPhone() {
			return phone;
		}

		public void setPhone(String phone) {
			this.phone = phone;
		}

		public Date getBirthdate() {
			return birthdate;
		}

		public void setBirthdate(Date birthdate) {
			this.birthdate = birthdate;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public int getZipcode() {
			return zipcode;
		}

		public void setZipcode(int zipcode) {
			this.zipcode = zipcode;
		}

		public String getVille() {
			return ville;
		}

		public void setVille(String ville) {
			this.ville = ville;
		}

		public String getPays() {
			return pays;
		}

		public void setPays(String pays) {
			this.pays = pays;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		@Override
		public String toString() {
			return "Client [id=" + id + ", name=" + name + ", firstname=" + firstname + ", mail=" + mail + ", phone="
					+ phone + ", birthdate=" + birthdate + ", address=" + address + ", zipcode=" + zipcode + ", ville="
					+ ville + ", pays=" + pays + ", user=" + user + "]";
		}

}
