/** @author Blondiaux Dorian
	@author Leclercq Jimmy 
	@author Group 4
 **/
package com.afpa.idp.classes;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;

import com.afpa.idp.proxmox.Client;
import com.afpa.idp.proxmox.Client.Result;

public class Service {
	private int serviceId;
	private String serviceName;
	//Ajouts VM
	private static String NODE ="sd-90297";
	private static String PROX_HOST = "sd-90297.dedibox.fr";
	private static int PROX_PORT = 8006;
	private static String PROX_USER = "afpa";
	private static String PROX_MDP = "Afpa_123";
	private Client client;
	private float price;
	private String vmType;

	//Si type est vrai, vm, sinon cloud
	/** @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 * @param serviceId  Id of the service
	 * @param serviceName Name of the service
	 * @param price Price of the service
	 * @param type Boolean, if true the service is a classical OS, else it is a cloud
	 * Constructor
	 */
	public Service(int serviceId, String serviceName, float price, boolean type) {
		this.serviceId=serviceId;
		this.serviceName=serviceName;
		this.price=price;
		if(type) {
			vmType="Qemu";
		}else {
			vmType="Lxc";
		}
	}

	/** @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 * @param serviceId  Id of the service
	 * @param serviceName Name of the service
	 * @param price Price of the service
	 * Constructor
	 */
	public Service(int serviceId, String serviceName, float price) {
		this.serviceId=serviceId;
		this.serviceName=serviceName;
		this.price=price;
	}
	
	/**
	 * @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 * Empty Constructor
	 */
	public Service() {}

	/** @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 **/
	@Override
	public String toString() {
		try {
			return "Service [serviceId=" + serviceId + ", serviceName=" + serviceName +", price="
					+ price + ", vmType=" + vmType + ", isOn()=" + isOn()  + "]";
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "Erreur";
	}

	/** @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 * Return true if the service is on else false
	 **/
	public boolean isOn() throws JSONException {
		boolean test;
		client = new Client(PROX_HOST, PROX_PORT);
		System.out.println(client.login(PROX_USER, PROX_MDP));

		if(vmType.contentEquals("Qemu")==false) {
			Result vms = client.getNodes().get(NODE).getLxc().get(serviceId).getStatus().getCurrent().getRest();

			test = vms.getResponse().getJSONObject("data").getString("status").contentEquals("running");
		}else {
			Result vms = client.getNodes().get(NODE).getQemu().get(serviceId).getStatus().getCurrent().getRest();
			test = vms.getResponse().getJSONObject("data").getString("status").contentEquals("running");
		}


		return test;
	}

	/** @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	 * Start the service if it's off then call lanchService() to launch it
	 **/
	public boolean startService() throws JSONException, InterruptedException, IOException, URISyntaxException {
		client = new Client(PROX_HOST, PROX_PORT);
		boolean test = client.login(PROX_USER, PROX_MDP);
		if(vmType.contentEquals("Qemu")) {
			client.getNodes().get(NODE).getQemu().get(serviceId).getStatus().getStart().vmStart();
		}else {
			client.getNodes().get(NODE).getLxc().get(serviceId).getStatus().getStart().vmStart();
		}
		//Wait serv is running
		while(!isOn()) {
			System.out.println("Please wait, service is starting ...");
			TimeUnit.SECONDS.sleep(1);
		}
		launchService();
		return test;
	}

	/** @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	Stop the service
	 **/
	public boolean stopService() throws JSONException {
		client = new Client(PROX_HOST, PROX_PORT);
		boolean test = client.login(PROX_USER, PROX_MDP);
		if(vmType.contentEquals("Qemu")) {
			client.getNodes().get(NODE).getQemu().get(serviceId).getStatus().getStop().vmStop();
		}else{
			client.getNodes().get(NODE).getLxc().get(serviceId).getStatus().getStop().vmStop();
		}
		return test;
	}

	/** @author Blondiaux Dorian
	 * @author Leclercq Jimmy 
	 * @author Group 4
	Launch the service, if the service is stopped, it starts it before launching it
	 **/
	public boolean launchService() throws JSONException, InterruptedException, IOException, URISyntaxException {
		if(isOn()) {
			// Lancer RDP
			if(getVmType().contentEquals("Lxc")) {
				Desktop.getDesktop().browse(new URI("https://localhost:"+ getServiceId()));
			}else {
				String path= System.getProperty("user.dir") + "\\conf\\"+getServiceId()+".bat";
				String pathTest= getPath()+getServiceId()+".bat";
				System.out.println(pathTest);

				PrintWriter writer = new PrintWriter(pathTest, "UTF-8");
				writer.println("mstsc /v:localhost:"+getServiceId()+" /admin");
				writer.close();

				Runtime runtime = Runtime.getRuntime();
				//String pathConfigFile ="C:/Users/afpa/Documents/101.rdp";
				runtime.exec(new String[] {"cmd", "/c", "Start",pathTest} );
			}
		}else {
			startService();
		}
		return true;
	}

	@Override public boolean equals(Object aThat) {
		if(this.serviceId==((Service) aThat).getServiceId()) {
			return true;
		}
		return false;

	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getVmType() {
		return vmType;
	}

	public void setVmType(String vmType) {
		this.vmType = vmType;
	}

	public String getPath() throws UnsupportedEncodingException {
		String path = this.getClass().getClassLoader().getResource("").getPath();
		String fullPath = URLDecoder.decode(path, "UTF-8");
		String pathArr[] = fullPath.split("/WEB-INF/classes/");
		fullPath = pathArr[0];
		String reponsePath = "";
		// to read a file from webcontent
		reponsePath = new File(fullPath).getPath() + File.separatorChar;
		return reponsePath;
	}


}
