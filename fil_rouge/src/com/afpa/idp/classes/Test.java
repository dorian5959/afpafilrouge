/**
 *  @author Blondiaux Dorian
		@author Leclercq Jimmy 
		@author Group 4
 */

package com.afpa.idp.classes;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.afpa.idp.dao.Bdd;
import com.afpa.idp.proxmox.*;
import com.afpa.idp.proxmox.Client.Result;

public class Test {
	


	public static void main(String[] args) throws IOException, JSONException, InterruptedException, URISyntaxException, ClassNotFoundException, SQLException {
	
		
				String port = "3006";
			    String base ="idp_services";
			    String url = "jdbc:mysql://localhost/"+ base;
			    Class.forName( "com.mysql.jdbc.Driver" );
				 Connection connexion = DriverManager.getConnection( url, "root", "" );
				 Scanner sc = new Scanner(System.in);
				 int choix;
				 
				//AVOIR UN USER "test" "test" dans la bdd et AVOIR LES TROIS SERVICES windows 10 windows serv cloud 
				User user = Bdd.findUser("test","test", connexion);
				//user = Bdd.findUser(1, connexion);
				
				//AJOUTER DES SUBCRIPTIONS SI BESOIN( enlever les commentaires
				/*
				Date date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
				Service service = Bdd.findService(101, connexion);
				Service service2 = Bdd.findService(105, connexion);
				Service service3 = Bdd.findService(106, connexion);
				Subscription subs = new Subscription(date, true, service, user);
				Subscription subs2 = new Subscription(date, true, service2, user);
				Subscription subs3 = new Subscription(date, true, service3, user);
				Bdd.insertSubscription(subs, connexion);
				Bdd.insertSubscription(subs2, connexion);
				Bdd.insertSubscription(subs3, connexion);*/
				
				//STOP SERVICE
				Service serv = Bdd.findService(101, connexion);
				serv.stopService();
				serv=Bdd.findService(105, connexion);
				serv.stopService();
				
				//LIRE LES SOUSCRIPTIONS ET UTILISER UN SERVICE
				
				
				ArrayList<Subscription> subscriptions = Bdd.getSubscriptions(user, connexion);
				user.setSubscriptions(subscriptions);
				int cmpt=1;
				
				for(Subscription sub: subscriptions) {
					System.out.println(cmpt +") " + sub.getService().getServiceName() +" " + sub.getUser().getUserName());
					cmpt++;
				}
				
				System.out.println("Selectionnez un numero de service:");
				choix=sc.nextInt();
				System.out.println(subscriptions.get(choix-1).getService().toString());
				
				System.out.println("Souhaitez vous utiliser le service ? (y/n)");
				String choix2=sc.next();
				
				if(choix2.contentEquals("y")) {
					subscriptions.get(choix-1).getService().launchService();
				}
				
				//SOUSCRIRE(ENLEVER COMMENTAIRE)
				//AVOIR UN USER "test2" "test2" dans la bdd et AVOIR LES TROIS SERVICES windows 10 windows serv cloud 
				
				/*
				User user2 = Bdd.findUser("test2","test2", connexion);
				ArrayList<Service> services = Bdd.findServices(connexion);
				int cmpt=1;
				for(Service service: services) {
					System.out.println(cmpt + ") " +service.toString());
					cmpt++;
				}
				System.out.println("Selectionnez un numero de service pour y souscrire:");
				choix=sc.nextInt();
				;
				//Ajoute la souscription dans le user et dans la base
				Bdd.insertSubscription(user2.subscribe(services.get(cmpt-2)), connexion);*/
		    }
		}