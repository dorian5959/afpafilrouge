package com.afpa.idp.classes;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import org.json.JSONException;

/**
 * @group 4
 * @author Leclercq Jimmy
 * @author Blondiaux Dorian
 */
public class User {
	private int userId;
	private String userName, password;
	private ArrayList<Subscription> subscriptions;

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 * @param userId
	 * @param userName
	 * @param password
	 * @param subscriptions
	 * Constructor
	 */
	public User(int userId, String userName, String password, ArrayList<Subscription> subscriptions) {
		this.userId=userId;
		this.userName=userName;
		this.password=password;
		this.subscriptions=subscriptions;
	}

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 * @param userId
	 * @param userName
	 * @param password
	 * Constructor
	 */
	public User(int userId, String userName, String password) {
		this.userId=userId;
		this.userName=userName;
		this.password=password;
		this.subscriptions=new ArrayList<Subscription>();
	}

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 * @param userName
	 * @param password
	 * Constructor
	 */
	public User(String userName, String password) {
		this.userName=userName;
		this.password=password;
		this.subscriptions=new ArrayList<Subscription>();
	}

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 * Constructor
	 */
	public User() {}
	
	//METHODS

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 */
	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", password=" + password + "]";
	}

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 * @param service The service to launch
	 * @return true
	 * @throws IOException
	 * @throws JSONException
	 * @throws InterruptedException
	 * @throws URISyntaxException
	 */
	public boolean launchService(Service service) throws IOException, JSONException, InterruptedException, URISyntaxException {

		//Check if the user has subscribed to this service
		if (isSubscribed(service)) {
			//Launch the service if it's off
			service.launchService();
			//Open RDP

		}
		return true;
	}

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 * @param service The service which the test check the client is subscribed to or not
	 * @return true if the client is subscribed, false if not
	 */
	public boolean isSubscribed(Service service) {
		boolean test=false;
		for(Subscription subs: subscriptions) {
			if(subs.getService().equals(service)) {
				test=true;
			}
		}
		return test;
	}

	/**
	 * @group 4
	 * @author Leclercq Jimmy
	 * @author Blondiaux Dorian
	 * @param service The service the client subscribe to
	 * @return The subscription
	 */
	public Subscription subscribe(Service service) {

		Date date = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		Subscription subscription = new Subscription(1,date, true, service, this);
		subscriptions.add(subscription);

		return subscription;
	}

	@Override public boolean equals(Object aThat) {
		if(this.userName.contentEquals(((User) aThat).getUserName())) {
			return true;
		}
		return false;

	}
	
	public int hashCode(){
		return this.userName.hashCode();//for simplicity reason
	}

	//GETTERS AND SETTERS

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public ArrayList<Subscription> getSubscriptions() {
		return subscriptions;
	}

	public void setSubscriptions(ArrayList<Subscription> subscriptions) {
		this.subscriptions = subscriptions;
	}

	public void addSubscription(Subscription subs) {
		subscriptions.add(subs);
	}

	public void deleteSubscription(Subscription subs) {
		subscriptions.remove(subs);
	}
}
