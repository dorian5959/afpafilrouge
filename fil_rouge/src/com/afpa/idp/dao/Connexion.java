package com.afpa.idp.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connexion {
	private int port;
	private String base;
	private String url;
	Connection connection;
	
	public Connexion() throws ClassNotFoundException, SQLException {
		super();
		this.port = 3006;
		this.base = "idp_services";
		this.url = "jdbc:mysql://localhost/"+ base;
		Class.forName( "com.mysql.jdbc.Driver" );
		connection = DriverManager.getConnection( url, "root", "" );
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	
}
