package com.afpa.idp.ext;

import java.util.Properties;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.FileInputStream;

public class ReadFile {
	
	static String value = "";

	   public static Properties load(String filename) throws IOException, FileNotFoundException{
	      Properties properties = new Properties(); 
	      FileInputStream input = new FileInputStream(filename); 
	      try{ 
	         properties.load(input);
	         return properties; 
	      } 
	              finally{ 
	         input.close(); 
	      } 
	   }
	   
	   /**
	    * Method static to read config.properties
	    * @author: Sofiane, Ndenga, Alex
	    * @param keyParam = la cl� dont on veut lire la valeur
	    * @return value corresponding to the parameter
	    */
	   
	   public static String readFile(String keyParam) {
		  
		   try{

		         Properties prop = ReadFile.load(".\\conf\\config.properties");
		         value = prop.getProperty(keyParam, "vide");
		      }
		      catch(Exception e){
		         e.printStackTrace();
		      }
		   return value;
	   }
	   
	   public static void main(String[] args) {
		   
		   readFile("com.mail.host");
		   System.out.println(value);
		   
	   }
	}
