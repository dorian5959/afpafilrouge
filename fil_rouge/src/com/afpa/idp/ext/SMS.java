package com.afpa.idp.ext;

/**
 * 
 */

import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.Properties;

/**
 * @author Fred Giordani
 *
 */

	public class SMS {
		
		
		// TODO Auto-generated method stub
		
		private static String msg;
		private static String msgEncode;
		        
		private static String URL;
		private static String USER;
		private static String PASS;
		private static String msgStart;
		private static String msgEnd;
		
		
		
		/**
		 * @Param msg the messsage to sent
		 * @Param msgEncode an empty string which will begin an encoded message
		 * @Param URL set the URL
		 * @Param USER  set the user
		 * @Param PASS  set the token
		 * @Param msgStart the beginning of encoded message
		 * @Param msgEnd  the end of encoded message
		 */
		
		public SMS( String msg,String msgEncode, String URL, String USER,String PASS,String msgStart, String msgEnd, String path) throws IOException{
				    
			SMS.msg = msg;
			SMS.msgEncode = msgEncode;
			
			// set URL, USER and PASS from a properties files
			SMS.URL  = Tools.readFile("com.SMS.URL", path);
            SMS.USER  = Tools.readFile("com.SMS.USER", path);
            SMS.PASS  = Tools.readFile("com.SMS.PASS", path);
			SMS.msgStart = "msg=";
			SMS.msgEnd = "%20!";
			
		}
		


		/**
		 * @return the msg
		 */
		public static String getMsg() {
			return msg;
		}


		/**
		 * @param msg the msg to set
		 */
		public static void setMsg(String msg) {
			SMS.msg = msg;
		}


		/**
		 * @return the msgEncode
		 */
		public static String getMsgEncode() {
			return msgEncode;
		}


		/**
		 * @param msgEncode the msgEncode to set
		 */
		public static void setMsgEncode(String msgEncode) {
			SMS.msgEncode = msgEncode;
		}


		/**
		 * @return the uRL
		 */
		public static String getURL() {
			return URL;
		}

		/**
		 * @param uRL the uRL to set
		 */
		public static void setURL(String uRL) {
			URL = uRL;
		}

		/**
		 * @return the uSER
		 */
		public static String getUSER() {
			return USER;
		}

		/**
		 * @param uSER the uSER to set
		 */
		public static void setUSER(String uSER) {
			USER = uSER;
		}

		/**
		 * @return the pASS
		 */
		public static String getPASS() {
			return PASS;
		}

		/**
		 * @param pASS the pASS to set
		 */
		public static void setPASS(String pASS) {
			PASS = pASS;
		}

		/**
		 * @return the msgStart
		 */
		public static String getMsgStart() {
			return msgStart;
		}

		/**
		 * @param msgStart the msgStart to set
		 */
		public static void setMsgStart(String msgStart) {
			SMS.msgStart = msgStart;
		}

		/**
		 * @return the msgEnd
		 */
		public static String getMsgEnd() {
			return msgEnd;
		}


		/**
		 * @param msgEnd the msgEnd to set
		 */
		public static void setMsgEnd(String msgEnd) {
			SMS.msgEnd = msgEnd;
		}

		/*
		 * @param msg the message to encode
		 * @param msgEncode an empty string which will become an encoded message
		 * @return an  encoded message
		 */
		public String encodeMessage(String msg, String msgEncode) {
			
			for (int i = 0; i < msg.length(); i++) {
				if( SMS.msg.charAt(i) != ' ') {
					SMS.msgEncode = SMS.msgEncode + SMS.msg.charAt(i);
				}else {
					SMS.msgEncode = SMS.msgEncode + "%20";
				}
				}
			
			return msgEncode;
			
		}
	    
}