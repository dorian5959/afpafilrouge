/**
 * 
 */
package com.afpa.idp.ext;

import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.Properties;

/*
 * @Author Camille, Fabrice, Fred
 */

	public class SmsFactory {
		
		
		// TODO Auto-generated method stub
		
		private static String msg;
		private static String msgEncode;
		        
		private static String URL;
		private static String USER;
		private static String PASS;
		private static String msgStart;
		private static String msgEnd;
		
		
		
		/*
		 * @Param msg the messsage to sent
		 * @Param msgEncode an empty string which will begin an encoded message
		 * @Param URL set the URL
		 * @Param USER  set the user
		 * @Param PASS  set the token
		 * @Param msgStart the beginning of encoded message
		 * @Param msgEnd  the end of encoded message
		 */
		
		public SmsFactory( String msg,String msgEncode, String URL, String USER,String PASS,String msgStart, String msgEnd) throws IOException{
			
			
			final Properties prop = new Properties();
			InputStream input = null;
			
			String dest= "";
			
	        
	        String path = System.getProperty("user.dir");
	        String osName = System.getProperty("os.name");
	        String file = "config.properties";
	        
	        
	        if (osName.equals("Mac OS X")) {
	        	dest = path + "/conf/" + file;
	        } else {
	        	dest = path + "\\conf\\" + file;
	        }
	        dest="C:\\Users\\afpa\\git\\fil_rouge_true\\fil_rouge\\conf\\config.properties";
		    input = new FileInputStream(dest);
		    
		    // load a properties file
		    prop.load(input);
		    
			SmsFactory.msg = msg;
			SmsFactory.msgEncode = msgEncode;
			
			// set URL, USER and PASS from a properties files
			SmsFactory.URL  = prop.getProperty("com.SMS.URL");
			SmsFactory.USER = prop.getProperty("com.SMS.USER");
			SmsFactory.PASS  = prop.getProperty("com.SMS.PASS");
//			SMS.URL  = Tools.readFile("com.SMS.URL");
//			SMS.USER  = Tools.readFile("com.SMS.USER");
//			SMS.PASS  = Tools.readFile("com.SMS.PASS");
			SmsFactory.msgStart = "msg=";
			SmsFactory.msgEnd = "%20.";
			
		}
		


		/**
		 * @return the msg
		 */
		public static String getMsg() {
			return msg;
		}


		/**
		 * @param msg the msg to set
		 */
		public static void setMsg(String msg) {
			SmsFactory.msg = msg;
		}


		/**
		 * @return the msgEncode
		 */
		public static String getMsgEncode() {
			return msgEncode;
		}


		/**
		 * @param msgEncode the msgEncode to set
		 */
		public static void setMsgEncode(String msgEncode) {
			SmsFactory.msgEncode = msgEncode;
		}


		/**
		 * @return the uRL
		 */
		public static String getURL() {
			return URL;
		}

		/**
		 * @param uRL the uRL to set
		 */
		public static void setURL(String uRL) {
			URL = uRL;
		}

		/**
		 * @return the uSER
		 */
		public static String getUSER() {
			return USER;
		}

		/**
		 * @param uSER the uSER to set
		 */
		public static void setUSER(String uSER) {
			USER = uSER;
		}

		/**
		 * @return the pASS
		 */
		public static String getPASS() {
			return PASS;
		}

		/**
		 * @param pASS the pASS to set
		 */
		public static void setPASS(String pASS) {
			PASS = pASS;
		}

		/**
		 * @return the msgStart
		 */
		public static String getMsgStart() {
			return msgStart;
		}

		/**
		 * @param msgStart the msgStart to set
		 */
		public static void setMsgStart(String msgStart) {
			SmsFactory.msgStart = msgStart;
		}

		/**
		 * @return the msgEnd
		 */
		public static String getMsgEnd() {
			return msgEnd;
		}


		/**
		 * @param msgEnd the msgEnd to set
		 */
		public static void setMsgEnd(String msgEnd) {
			SmsFactory.msgEnd = msgEnd;
		}

		/*
		 * @param msg the message to encode
		 * @param msgEncode an empty string which will become an encoded message
		 * @return an  encoded message
		 */
		public String encodeMessage(String msg, String msgEncode) {
			
			for (int i = 0; i < msg.length(); i++) {
				if( SmsFactory.msg.charAt(i) != ' ') {
					SmsFactory.msgEncode = SmsFactory.msgEncode + SmsFactory.msg.charAt(i);
				}else {
					SmsFactory.msgEncode = SmsFactory.msgEncode + "%20";
				}
				}
			
			return msgEncode;
			
		}
	    
		
		/*
		 * send an SMS to the user
		 */
		public static void sendSMS() throws IOException {
				
			
				
				SmsFactory sms  = new SmsFactory("vous etes abonne a nos service. Pour des infos complementaire reportez vous � votre facture","",URL,USER,PASS,msgStart,msgEnd);
				

				
				sms.encodeMessage(SmsFactory.getMsg(),SmsFactory.getMsgEncode());
				
				String message = SmsFactory.getMsgEncode();
				
				
				
				URL url = new URL( SmsFactory.getURL() + SmsFactory.getUSER() + SmsFactory.getPASS() + SmsFactory.getMsgStart() + message + SmsFactory.getMsgEnd() );
				
				// open the connection for the url
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				connection.setRequestMethod("GET");
				connection.connect();
				
				// get the response code
				int code = connection.getResponseCode();
				
				// display the response code
				System.out.println(code);
				
			
		}

		
}
	
