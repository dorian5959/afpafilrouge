/**
 * @author Group 4
 */
package com.afpa.idp.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/deconnected.do")
public class Deconnection extends HttpServlet {

	/**
	 * @param request HttpRequest
	 * @param response HttpResponse
	 */
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
		HttpSession session = request.getSession();
		session.removeAttribute("user");
		request.setAttribute("request","decotrue");
		this.getServletContext().getRequestDispatcher( "/WEB-INF/isaction.jsp" ).forward( request, response );
	}

	/**
	 * @param request HttpRequest
	 * @param response HttpResponse
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher("/WEB-INF/isaction.jsp").forward(request, response);
	}
}