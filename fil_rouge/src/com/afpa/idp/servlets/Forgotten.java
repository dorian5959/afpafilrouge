/**
 * @author Group 4
 */
package com.afpa.idp.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.afpa.idp.classes.Client;
import com.afpa.idp.classes.User;
import com.afpa.idp.dao.ClientDAO;
import com.afpa.idp.dao.Connexion;
import com.afpa.idp.dao.DAO;
import com.afpa.idp.ext.Password;
import com.afpa.idp.ext.PdfFactory;
import com.afpa.idp.ext.Tools;

/**
 * Servlet implementation class Forgotten
 */
@WebServlet("/forgot.do")
public class Forgotten extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Forgotten() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * @param request HttpRequest
	 * @param response HttpResponse
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		this.getServletContext().getRequestDispatcher( "/WEB-INF/forgotten.jsp" ).forward( request, response );
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * @param request HttpRequest
	 * @param response HttpResponse
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Connexion co = new Connexion();
			Connection connexion = co.getConnection();

			//HttpSession req = request.getSession();

			DAO<Client> clientDao = new ClientDAO(connexion);
			
			
			String email = request.getParameter( "email" );  
			String login = request.getParameter( "login" );
			
			System.out.println(email+ " " + login);
			
			Client cli = clientDao.find(login);
			
			if (cli!=null && email.contentEquals(cli.getMail()) && login.contentEquals(cli.getUser().getUserName())) {
				
				request.setAttribute("request", "forgottentrue");
				String newPassword = generate(5); //generate a random string with 5 as length
				Tools.sendMail(cli.getMail(), "Votre nouveau mot de passe", "Bonjour, voici votre nouveau mot de passe : "+newPassword, "C:\\Users\\afpa\\git\\fil_rouge_true\\fil_rouge\\conf");
				newPassword = Password.hashPassword(newPassword); //Hash the password before the user update in database
				cli.getUser().setPassword(newPassword);
				clientDao.update(cli);
			}else {
				request.setAttribute("request", "forgottenfalse");
			}
			
			this.getServletContext().getRequestDispatcher( "/WEB-INF/isaction.jsp" ).forward( request, response );
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * @param length Length of string to generate
	 * @return pass Random String of length length
	 */
	public String generate(int length)
	{
	        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; // Tu supprimes les lettres dont tu ne veux pas
	        String pass = "";
	        for(int x=0;x<length;x++)
	        {
	           int i = (int)Math.floor(Math.random() * 62); // Si tu supprimes des lettres tu diminues ce nb
	           pass += chars.charAt(i);
	        }
	        System.out.println(pass);
	        return pass;
	}
}