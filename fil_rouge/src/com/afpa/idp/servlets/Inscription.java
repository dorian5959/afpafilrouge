package com.afpa.idp.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.afpa.idp.classes.Client;
import com.afpa.idp.classes.User;
import com.afpa.idp.dao.ClientDAO;
import com.afpa.idp.dao.Connexion;
import com.afpa.idp.dao.DAO;
import com.afpa.idp.dao.UserDAO;
import com.afpa.idp.ext.Password;
import com.afpa.idp.ext.Tools;

@WebServlet("/inscription.do")
public class Inscription extends HttpServlet {

	private static final long serialVersionUID = -5593779107270821692L;

	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
				
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/inscription.jsp" ).forward( request, response );
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		  
		  String login = request.getParameter( "login" );
		  String password = request.getParameter( "Password" );
		  password = Password.hashPassword(password);
		  //Hash password
		/*  try {
			password = HashPassword.getSecurePassword(password, HashPassword.getSalt());
		} catch (NoSuchAlgorithmException | NoSuchProviderException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}*/
		  
		  String name = request.getParameter( "Name" );
		  String firstName = request.getParameter( "Firstname" );
		  String userMail = request.getParameter( "user_mail" );
		  String address = request.getParameter( "address" );
		  String birthday = request.getParameter( "Birthday" );
		  String city = request.getParameter( "city" );
		  String phone = request.getParameter("phone");
		  
		  String postalCode = request.getParameter( "postal-code" );
		  String country = request.getParameter( "country" );
		
		  User user = new User(login,password);
		  
		  SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
		  java.util.Date date1;
		
		  
			try {
				Connexion co = new Connexion();
				DAO<User> userDAO = new UserDAO(co.getConnection());
				if(userDAO.find().contains(user)==false) {
					birthday.replaceAll("/", "-");
					date1 = dt1.parse(birthday);
					Client cli = new Client(name, firstName, userMail, phone, date1, address, Integer.parseInt(postalCode), city, country, user);
					
					
					DAO<Client> clientDAO = new ClientDAO(co.getConnection());
					cli=clientDAO.create(cli);
					Tools.writeLogs(cli.getUser().getUserId(),"INSCRIPTION: ", cli.toString());
					Tools.sendMail(cli.getMail(),"Votre inscription " + cli.getFirstname() +" " + cli.getName(),"Bonjour, votre inscription a bien �t� prise en compte","C:\\Users\\afpa\\git\\fil_rouge_true\\fil_rouge\\conf");
					
					request.setAttribute("request", "inscritrue");
				     this.getServletContext().getRequestDispatcher("/WEB-INF/isaction.jsp").forward(request, response);
				  }else {
					  request.setAttribute("request", "inscrifalse");
					  this.getServletContext().getRequestDispatcher("/WEB-INF/isaction.jsp").forward(request, response);
				  }  
			} catch (ParseException | ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	
