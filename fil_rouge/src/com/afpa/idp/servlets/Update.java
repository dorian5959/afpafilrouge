/**
 * @author Group 4
 */
package com.afpa.idp.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.HttpRequest;

import com.afpa.idp.classes.Client;
import com.afpa.idp.classes.User;
import com.afpa.idp.dao.ClientDAO;
import com.afpa.idp.dao.Connexion;
import com.afpa.idp.dao.DAO;
import com.afpa.idp.dao.UserDAO;
import com.afpa.idp.ext.Password;
import com.afpa.idp.dao.Connexion;

/**
 * Servlet implementation class Update
 */
@WebServlet("/update.do")
public class Update extends HttpServlet {

	private static final long serialVersionUID = 1095678989278654578L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * @param request HttpRequest
	 * @param response HttpResponse
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session= request.getSession();
		if(session.getAttribute("user")!=null) {
			Connexion co;
			try {
				co = new Connexion();
				Connection connexion = co.getConnection();
				session = request.getSession();
				DAO<Client> clientDao = new ClientDAO(connexion);
				User user =(User) session.getAttribute("user");
				Client cli = clientDao.find(user.getUserId());
				request.setAttribute("client", cli);
				
				this.getServletContext().getRequestDispatcher( "/WEB-INF/update.jsp" ).forward( request, response );
				
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else {
			
			request.setAttribute("request", "connectedfalse");
			this.getServletContext().getRequestDispatcher( "/WEB-INF/isaction.jsp" ).forward( request, response );
		}
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/**
	 * @param request HttpRequest
	 * @param response HttpResponse
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Connexion co = new Connexion();
			Connection connexion = co.getConnection();
			
			HttpSession session = request.getSession();
			
			DAO<Client> clientDao = new ClientDAO(connexion);
			Client cli = clientDao.find(((User) (session.getAttribute("user"))).getUserId());
			
			
			String login = request.getParameter( "login" );
			
			

			String name = request.getParameter( "Name" );
			String firstName = request.getParameter( "Firstname" );
			String userMail = request.getParameter( "user_mail" );
			String address = request.getParameter( "address" );
			String birthday = request.getParameter( "Birthday" );
			birthday.replaceAll("/", "-");
			String city = request.getParameter( "city" );
			String phone = request.getParameter("phone");

			String postalCode = request.getParameter( "postal-code" );
			String country = request.getParameter( "country" );

			SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date date1;
			date1 = dt1.parse(birthday);
			
			cli.getUser().setUserName(login);
			String password = request.getParameter( "Password" );
			if(password != null && password.contentEquals("")==false){
				password = Password.hashPassword(password);
				cli.getUser().setPassword(password);
				
			}else {
			}
		
			cli.setName(name);
			cli.setFirstname(firstName);
			cli.setMail(userMail);
			cli.setAddress(address);
			cli.setBirthdate(date1);
			cli.setVille(city);
			cli.setPhone(phone);
			cli.setZipcode(Integer.parseInt(postalCode));
			cli.setPays(country);
			System.out.println(cli.toString());

			cli=clientDao.update(cli);
			request.setAttribute("client", cli);
			
			doGet(request, response);
			//this.getServletContext().getRequestDispatcher( "/WEB-INF/update.jsp" ).forward( request, response );
		} catch (SQLException | ParseException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}